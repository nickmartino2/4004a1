package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import server.logic.model.Fee;

public class FeeTests {

	@Test
	public void TestCreateFee() {
		int userID = 4;
		int feeValue = 6;
		
		Fee fee = new Fee(userID, feeValue);
		
		assertEquals(userID, fee.getUserid());
		assertEquals(feeValue, fee.getFee());
	}
	
	@Test
	public void TestUserID() {
		int userID = 4;
		int feeValue = 6;
		
		Fee fee = new Fee(0, feeValue);
		fee.setUserid(userID);
		
		assertEquals(userID, fee.getUserid());
	}
	
	@Test
	public void TestFee() {
		int userID = 4;
		int feeValue = 6;
		
		Fee fee = new Fee(userID, 0);
		fee.setFee(feeValue);
		
		assertEquals(feeValue, fee.getFee());
	}
	
	@Test
	public void TestToString() {
		int userID = 4;
		int feeValue = 6;
		String returnString = "["+userID+","+feeValue+"]";
		
		Fee fee = new Fee(userID, feeValue);
		
		assertEquals(returnString, fee.toString());
	}
	
	

}
