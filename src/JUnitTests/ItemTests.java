package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import server.logic.model.Item;

public class ItemTests {

	@Test
	public void TestCreateItem() {
		int itemID = 3;
		String ISBN = "500";
		String copynumber = "999";
		
		Item item = new Item(itemID, ISBN, copynumber);
		
		assertEquals(itemID, item.getItemid());
		assertEquals(ISBN, item.getISBN());
		assertEquals(copynumber, item.getCopynumber());
	}
	
	@Test
	public void TestItemID() {
		int itemID = 3;
		String ISBN = "500";
		String copynumber = "999";
		
		Item item = new Item(0, ISBN, copynumber);
		item.setItemid(itemID);
		
		assertEquals(itemID, item.getItemid());
	}
	
	@Test
	public void TestIBSN() {
		int itemID = 3;
		String ISBN = "500";
		String copynumber = "999";
		
		Item item = new Item(itemID, "", copynumber);
		item.setISBN(ISBN);
		
		assertEquals(ISBN, item.getISBN());
	}
	
	@Test
	public void TestCopynumber() {
		int itemID = 3;
		String ISBN = "500";
		String copynumber = "999";
		
		Item item = new Item(itemID, ISBN, "");
		item.setCopynumber(copynumber);
		
		assertEquals(copynumber, item.getCopynumber());
	}
	
	@Test
	public void TestToString() {
		int itemID = 3;
		String ISBN = "500";
		String copynumber = "999";
		String returnString = "["+itemID+","+ISBN+","+copynumber+"]";
		
		Item item = new Item(itemID, ISBN, copynumber);
		
		assertEquals(returnString, item.toString());
	}

}
