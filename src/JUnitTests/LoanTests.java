package JUnitTests;

import static org.junit.Assert.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import server.logic.model.Loan;

public class LoanTests {

	@Test
	public void TestCreateLoan() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		
		
		Loan loan = new Loan(userID, ISBN, copynumber, date, renewState);
		
		assertEquals(userID, loan.getUserid());
		assertEquals(ISBN, loan.getIsbn());
		assertEquals(copynumber, loan.getCopynumber());
		assertEquals(date, loan.getDate());
		assertEquals(renewState, loan.getRenewstate());
	}
	
	@Test
	public void TestUserID() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		
		
		Loan loan = new Loan(0, ISBN, copynumber, date, renewState);
		loan.setUserid(userID);
		
		assertEquals(userID, loan.getUserid());
	}
	
	@Test
	public void TestIBSN() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		
		
		Loan loan = new Loan(userID, "", copynumber, date, renewState);
		loan.setIsbn(ISBN);
		
		assertEquals(ISBN, loan.getIsbn());
	}
	
	@Test
	public void TestCopynumber() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		
		
		Loan loan = new Loan(userID, ISBN, "", date, renewState);
		loan.setCopynumber(copynumber);
		
		assertEquals(copynumber, loan.getCopynumber());
	}
	
	@Test
	public void TestDate() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		
		
		Loan loan = new Loan(userID, ISBN, copynumber, null, renewState);
		loan.setDate(date);
		
		assertEquals(date, loan.getDate());
	}
	
	@Test
	public void TestRenewState() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		
		
		Loan loan = new Loan(userID, ISBN, copynumber, date, "");
		loan.setRenewstate(renewState);
		
		assertEquals(renewState, loan.getRenewstate());
	}
	
	@Test
	public void TestToString() {
		int userID = 3;
		String ISBN = "500";
		String copynumber = "999";
		Date date = new Date();
		String renewState = "1000";
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String returnString = "["+userID+","+ISBN+","+copynumber+","+format1.format(date)+","+renewState+"]";
		
		
		Loan loan = new Loan(userID, ISBN, copynumber, date, renewState);
		
		assertEquals(returnString, loan.toString());
	}

}
