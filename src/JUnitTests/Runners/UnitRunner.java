package JUnitTests.Runners;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import JUnitTests.FeeTests;
import JUnitTests.ItemTests;
import JUnitTests.LoanTests;
import JUnitTests.TitleTests;
import JUnitTests.UserTests;
import JUnitTests.Tables.FeeTableTests;
import JUnitTests.Tables.ItemTableTests;
import JUnitTests.Tables.LoanTableTests;
import JUnitTests.Tables.TitleTableTests;
import JUnitTests.Tables.UserTableTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({FeeTests.class, ItemTests.class, LoanTests.class, TitleTests.class, UserTests.class, FeeTableTests.class, ItemTableTests.class, LoanTableTests.class, TitleTableTests.class, UserTableTests.class})
public final class UnitRunner{}
