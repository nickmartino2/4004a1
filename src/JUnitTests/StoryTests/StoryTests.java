package JUnitTests.StoryTests;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.model.Item;
import server.logic.model.Loan;
import server.logic.model.Title;
import server.logic.model.User;
import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class StoryTests {

	@Before
	public void setUp() throws Exception {
		LoanTable.resetInstance();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void UserStory1() {
		ItemTable table = ItemTable.getInstance();
		
		Item item = table.getItem("9781442668584");
		
		assertEquals(0, item.getItemid());
		assertEquals("9781442668584", item.getISBN());
		assertEquals("1", item.getCopynumber());
	}
	
	@Test
	public void UserStory1FailRule() {
		ItemTable table = ItemTable.getInstance();
		
		Item item = table.getItem("NOTEXIST");
		
		assertEquals(null, item);
	}

	@Test
	public void UserStory2() {
		ItemTable table = ItemTable.getInstance();
		
		boolean madeItem = (boolean) table.createitem("NOTREAL");
		
		assertEquals(false, madeItem);
	}
	
	@Test
	public void UserStory3() {
		UserTable table = UserTable.getInstance();
		
		boolean madeUser = (boolean) table.createuser("nick50@email.com", "password2");
		
		assertEquals(true, madeUser);
	}
	
	@Test
	public void UserStory3FailRule() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@email.com", "password");
		boolean madeUser = (boolean) table.createuser("nick@email.com", "password");
		
		assertEquals(false, madeUser);
	}
	
	@Test
	public void UserStory4() {
		TitleTable table = TitleTable.getInstance();
		
		boolean madeTitle = (boolean) table.createtitle("NewOne", "The Newest");
		
		assertEquals(true, madeTitle);
	}
	
	@Test
	public void UserStory4FailRule() {
		TitleTable table = TitleTable.getInstance();
		
		table.createtitle("LOTR", "Lord of the Rings");
		boolean madeTitle = (boolean) table.createtitle("LOTR", "Lord of the Rings");
		
		assertEquals(false, madeTitle);
	}
	
	@Test
	public void UserStory5() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@mail.com", "password");
		User user = table.getUser("nick@mail.com");
		
		
		assertEquals(7, user.getUserid());
		assertEquals("nick@mail.com", user.getUsername());
		assertEquals("password", user.getPassword());
	}
	
	@Test
	public void UserStory5FailRule() {
		UserTable table = UserTable.getInstance();
		
		User user = table.getUser("nick@mail.com");
		
		
		assertEquals(null, user);
	}
	
	@Test
	public void UserStory6() {
		TitleTable table = TitleTable.getInstance();
		
		table.createtitle("100900", "Look, a Book Title!");
		Title title = table.getTitle("100900");

		assertEquals("Look, a Book Title!", title.getBooktitle());
		assertEquals("100900", title.getISBN());
	}
	
	@Test
	public void UserStory6FailRule1() {
		TitleTable table = TitleTable.getInstance();
		
		Title title = table.getTitle("NOTEXIST");

		assertEquals(null, title);
	}
	
	@Test
	public void UserStory7() {
		LoanTable table = LoanTable.getInstance();
		
		FeeTable.getInstance().payfine(1);
		String result = (String) table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("success", result);
	}
	
	@Test
	public void UserStory7FailRule1() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertNotEquals("successs", result);
	}
	
	@Test
	public void UserStory7FailRule2() {
		LoanTable table = LoanTable.getInstance();
		
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		String result = (String) table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
			
		assertNotEquals("success", result);
	}
	
	@Test
	public void UserStory8() {
		FeeTable table = FeeTable.getInstance();
		
		String result = (String) table.payfine(0);
		
		assertNotEquals("success", result);
	}
	
	@Test
	public void UserStory8FailRule() {
		FeeTable table = FeeTable.getInstance();
		
		String result = (String) table.payfine(0);
		
		assertNotEquals("success", result);
	}
	
	@Test
	public void UserStory9() {
		ItemTable table = ItemTable.getInstance();
		
		String result = (String) table.delete("9781611687910", "1");
		
		assertEquals("success", result);
	}
	
	@Test
	public void UserStory9FailRule1() {
		ItemTable table = ItemTable.getInstance();
		
		String result = (String) table.delete("NOTREAL", "4004");
		
		assertEquals("The Item Does Not Exist", result);
	}
	
	@Test
	public void UserStory9FailRule2() {
		ItemTable table = ItemTable.getInstance();
		
		String result = (String) table.delete("9781442668584", "1");
		
		assertEquals("Active Loan Exists", result);
	}
	
	@Test
	public void UserStory10() {
		TitleTable table = TitleTable.getInstance();
		
		table.createtitle("LOTR", "Lord of the Rings");
		String deleteResult = (String)table.delete("LOTR");
		
		assertEquals("success", deleteResult);
	}
	
	@Test
	public void UserStory10FailRule1() {
		TitleTable table = TitleTable.getInstance();
		
		String deleteResult = (String)table.delete("NotThere");
		
		assertEquals("The Title Does Not Exist", deleteResult);
	}
	
	@Test
	public void UserStory10FailRule2() {
		TitleTable table = TitleTable.getInstance();
		
		String deleteResult = (String)table.delete("9781442668584");
		
		assertEquals("Active Loan Exists", deleteResult);
	}
	
	@Test
	public void UserStory11() {
		UserTable table = UserTable.getInstance();
		
		String result = (String) table.delete(2);
		
		assertEquals("success", result);
	}
	
	@Test
	public void UserStory11FailRule1() {
		UserTable table = UserTable.getInstance();
		
		String result = (String) table.delete(12);
		
		assertEquals("The User Does Not Exist", result);
	}
	
	@Test
	public void UserStory11FailRule2() {
		UserTable table = UserTable.getInstance();
		
		String result = (String) table.delete(0);
		
		assertEquals("Active Loan Exists", result);
	}
	
	@Test
	public void UserStory12() {
		LoanTable table = LoanTable.getInstance();
		
		
		Loan loan = table.getLoan("9781442668584");
		
		assertEquals(0, loan.getUserid());
		assertEquals("9781442668584", loan.getIsbn());
		assertEquals("1", loan.getCopynumber());
		assertEquals("0", loan.getRenewstate());
	}
	
	@Test
	public void UserStory12FailRule() {
		LoanTable table = LoanTable.getInstance();
		
		Loan loan = table.getLoan("NOTEXIST");
		
		assertEquals(null, loan);
	}
	
	@Test
	public void UserStory13() {
		LoanTable table = LoanTable.getInstance();
		
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		String result = (String) table.renewal(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("success", result);
	}
	
	@Test
	public void UserStory13FailRule1() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.renewal(3, "NOTEXIST", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("The loan does not exist", result);
	}
	
	@Test
	public void UserStory13FailRule2() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.renewal(0, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertNotEquals("success", result);
	}
	
	@Test
	public void UserStory13FailRule3() {
		LoanTable table = LoanTable.getInstance();
		
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.renewal(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		String result = (String) table.renewal(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("Renewed Item More Than Once for the Same Loan", result);
	}
	
	@Test
	public void UserStory14() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.returnItem(0, "9781442668584", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("success", result);
	}
	
	@Test
	public void UserStory14FailRule() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.returnItem(0, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("The Loan Does Not Exist", result);
	}
	
	@Test
	public void UserStory15() {
		TitleTable table1 = TitleTable.getInstance();
		UserTable table2 = UserTable.getInstance();
		
		List<Title> titles = table1.getTitleTable();
		List<User> users = table2.getUserTable();
		
		assertEquals("[9781442668584,By the grace of God]", titles.get(0).toString());
		assertEquals("[9781442616899,Dante's lyric poetry ]", titles.get(1).toString());
		assertEquals("[9781442667181,Courtesy lost]", titles.get(2).toString());
		assertEquals("[9781611687910,Writing for justice]", titles.get(3).toString());
		assertEquals("[9781317594277,The act in context]", titles.get(4).toString());
		assertEquals("[NewOne,The Newest]", titles.get(5).toString());
		assertEquals("[100900,Look, a Book Title!]", titles.get(6).toString());
		
		
		assertEquals("[0,Zhibo@carleton.ca,Zhibo]", users.get(0).toString());
		assertEquals("[1,Yu@carleton.ca,Yu]", users.get(1).toString());
		assertEquals("[2,N/A,N/A]", users.get(2).toString());
		assertEquals("[3,Kevin@carleton.ca,Kevin]", users.get(3).toString());
		assertEquals("[4,Sun@carleton.ca,Sun]", users.get(4).toString());
		assertEquals("[5,nick@email.com,password]", users.get(5).toString());
		assertEquals("[6,nick50@email.com,password2]", users.get(6).toString());
		assertEquals("[7,nick@mail.com,password]", users.get(7).toString());
	}

}
