package JUnitTests.Tables;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import server.logic.model.Fee;
import server.logic.tables.FeeTable;

public class FeeTableTests {
	
	@Test
	public void TestLookupNoFee() {
		FeeTable table = FeeTable.getInstance();
		
		boolean hasFee = table.lookup(0);
		
		assertEquals(false, hasFee);
	}
	
	@Test
	public void TestLookupHasFee() {
		FeeTable table = FeeTable.getInstance();
		
		boolean hasFee = table.lookup(0);
		
		assertEquals(true, hasFee);
	}
	
	@Test
	public void TestLookupFeeNoFee() {
		FeeTable table = FeeTable.getInstance();
		
		int fee = (int) table.lookupfee(2);
		
		assertEquals(fee, 0);
	}
	
	@Test
	public void TestLookupFeeHasFee() {
		FeeTable table = FeeTable.getInstance();
		
		int fee = (int) table.lookupfee(0);
		
		assertEquals(fee, 5);
	}
	
	@Test
	public void TestApplyFeeToExisting() {
		FeeTable table = FeeTable.getInstance();
		
		table.applyfee(0, 25119067);
		
		int fee = (int) table.lookupfee(0);
		
		assertEquals(fee, 413);
	}
	
	@Test
	public void TestApplyFeeToNewUser() {
		FeeTable table = FeeTable.getInstance();
		
		table.applyfee(1, 25119067);
		
		int fee = (int) table.lookupfee(1);
		
		assertEquals(fee, 413);
	}
	
	@Test
	public void TestInitialization() {
		FeeTable table = FeeTable.getInstance();
		
		table.Initialization();
		
		List<Fee> feeTable = table.getFeeTable();
		
		Fee sampleFee = new Fee(0,5);
		
		assertEquals(feeTable.get(0).toString(), sampleFee.toString());
	}
	
	@Test
	public void TestPayFineHasUser() {
		FeeTable table = FeeTable.getInstance();
		
		String result = (String) table.payfine(1);
		
		assertEquals(result, "success");
	}
	
	@Test
	public void TestPayFineNoUser() {
		FeeTable table = FeeTable.getInstance();
		
		String result = (String) table.payfine(2);
		
		assertEquals(result, "success");
	}
	
	@Test
	public void TestPayFineOLoanBlock() {
		FeeTable table = FeeTable.getInstance();
		
		String result = (String) table.payfine(0);
		
		assertEquals(result, "Borrowing Items Exist");
	}
	
	

}
