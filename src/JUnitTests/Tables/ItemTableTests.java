package JUnitTests.Tables;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.TitleTable;

public class ItemTableTests {

	@Test
	public void TestCreateItem() {
		ItemTable table = ItemTable.getInstance();
		
		TitleTable.getInstance().createtitle("NewThing1", "2");
		boolean madeItem = (boolean) table.createitem("NewThing1");
		
		assertEquals(true, madeItem);
	}
	
	@Test
	public void TestCreateItemFail() {
		ItemTable table = ItemTable.getInstance();
		
		boolean madeItem = (boolean) table.createitem("NOTREAL");
		
		assertEquals(false, madeItem);
	}
	
	@Test
	public void TestLookup() {
		ItemTable table = ItemTable.getInstance();
		
		boolean foundItem = (boolean) table.lookup("9781442668584", "1");
		
		assertEquals(true, foundItem);
	}
	
	@Test
	public void TestLookupFailCopyNumber() {
		ItemTable table = ItemTable.getInstance();
		
		boolean foundItem = (boolean) table.lookup("9781442668584", "-1000");
		
		assertEquals(false, foundItem);
	}
	
	@Test
	public void TestLookupFailISBN() {
		ItemTable table = ItemTable.getInstance();
		
		boolean foundItem = (boolean) table.lookup("NOTREAL", "1");
		
		assertEquals(false, foundItem);
	}
	
	@Test
	public void TestDelete() {
		ItemTable table = ItemTable.getInstance();
		
		String result = (String) table.delete("9781611687910", "1");
		
		assertEquals("success", result);
	}
	
	@Test
	public void TestDeleteFailDNE() {
		ItemTable table = ItemTable.getInstance();
		
		String result = (String) table.delete("NOTREAL", "4004");
		
		assertEquals("The Item Does Not Exist", result);
	}
	
	@Test
	public void TestDeleteFailOnLoan() {
		ItemTable table = ItemTable.getInstance();
		
		String result = (String) table.delete("9781442668584", "1");
		
		assertEquals("Active Loan Exists", result);
	}
	
	@Test
	public void TestDeleteAll() {
		ItemTable table = ItemTable.getInstance();
		
		table.deleteAll("LOTR");
		
		assertEquals(false, table.lookup("LOTR", "4004"));
	}

}
