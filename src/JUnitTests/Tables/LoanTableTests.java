package JUnitTests.Tables;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import server.logic.model.Fee;
import server.logic.model.Loan;
import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.UserTable;

public class LoanTableTests {

	@Before
	public void setUp() throws Exception {
		LoanTable.resetInstance();
	}

	@After
	public void tearDown() throws Exception {
		LoanTable.resetInstance();
	}
	
	@Test
	public void TestCreateLoanSuccess() {
		LoanTable table = LoanTable.getInstance();
		
		FeeTable.getInstance().payfine(1);
		String result = (String) table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("success", result);
	}
	
	@Test
	public void TestCreateLoanInvalidUser() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.createloan(999, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("User Invalid", result);
	}
	
	@Test
	public void TestCreateLoanInvalidISBN() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.createloan(0, "NotRealForSure", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("ISBN Invalid", result);
	}
	
	@Test
	public void TestCreateLoanInvalidCopynumber() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.createloan(0, "9781442616899", "-1000", new Date(System.currentTimeMillis()));
		
		assertEquals("Copynumber Invalid", result);
	}
	
	@Test
	public void TestCreateLoanOutstandingFee() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.createloan(0, "9781442667181", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("Outstanding Fee Exists", result);
	}
	
	@Test
	public void TestCreateLoanItemNotAvailable() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.createloan(1, "9781442668584", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("The Item is Not Available", result);
	}
	
	@Test
	public void TestLookupFails() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.lookup(0, "9781442668584", "1");
		
		assertEquals(false, result);
	}
	
	@Test
	public void TestLookupSucceeds() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.lookup(0, "9781442616899", "1");
		
		assertEquals(true, result);
	}
	
	@Test
	public void TestCheckLimitAtLimit() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkLimit(0);
		
		assertEquals(true, result);
	}
	
	@Test
	public void TestCheckLimitBelowLimit() {
		LoanTable table = LoanTable.getInstance();
		
		table.returnItem(0, "9781442668584", "1", new Date(System.currentTimeMillis()));
		
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442667181", "1", new Date(System.currentTimeMillis()));
		table.createloan(1, "9781442668584", "1", new Date(System.currentTimeMillis()));
		
		boolean result = table.checkLimit(1);
		
		assertEquals(false, result);
	}
	
	@Test
	public void TestRenewalSuccess() {
		LoanTable table = LoanTable.getInstance();
		
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		String result = (String) table.renewal(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("success", result);
	}
	
	@Test
	public void TestRenewalMoreThanOnceSameLoan() {
		LoanTable table = LoanTable.getInstance();
		
		table.createloan(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		table.renewal(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		String result = (String) table.renewal(1, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("Renewed Item More Than Once for the Same Loan", result);
	}
	
	@Test
	public void TestRenewalLoanDoesNotExist() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.renewal(3, "NOTEXIST", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("The loan does not exist", result);
	}
	
	@Test
	public void TestRenewalHasOutstandingFee() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.renewal(0, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("Outstanding Fee Exists", result);
	}
	
	@Test
	public void TestReturnItemSuccess() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.returnItem(0, "9781442668584", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("success", result);
	}
	
	@Test
	public void TestReturnItemLoanDoesNotExist() {
		LoanTable table = LoanTable.getInstance();
		
		String result = (String) table.returnItem(0, "9781442616899", "1", new Date(System.currentTimeMillis()));
		
		assertEquals("The Loan Does Not Exist", result);
	}
	
	@Test
	public void TestLoanTable() {
		LoanTable table = LoanTable.getInstance();
		
		List<Loan> loanTable = table.getLoanTable();
		
		Loan sampleLoan = new Loan(0,"9781442668584","1",new Date(),"0");
		
		assertEquals(loanTable.get(0).toString(), sampleLoan.toString());
	}
	
	@Test
	public void TestLookLimitSuccess() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.looklimit(2);
		
		assertEquals(true, result);
	}
	
	@Test
	public void TestLookLimitFail() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.looklimit(0);
		
		assertEquals(false, result);
	}
	
	@Test
	public void TestCheckUserSuccess() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkUser(1);
		
		assertEquals(true, result);
	}
	
	@Test
	public void TestCheckUserFail() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkUser(0);
		
		assertEquals(false, result);
	}
	
	@Test
	public void TestCheckLoanSuccessTwoParam() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkLoan("9781442616899", "1");
		
		assertEquals(true, result);
	}
	
	@Test
	public void TestCheckLoanFailTwoParam() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkLoan("9781442668584", "1");
		
		assertEquals(false, result);
	}
	
	@Test
	public void TestCheckLoanSuccessOneParam() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkLoan("9781442616899");
		
		assertEquals(true, result);
	}
	
	@Test
	public void TestCheckLoanFailOneParam() {
		LoanTable table = LoanTable.getInstance();
		
		boolean result = table.checkLoan("9781442668584");
		
		assertEquals(false, result);
	}

}
