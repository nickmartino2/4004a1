package JUnitTests.Tables;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class TitleTableTests {

	@Test
	public void TestCreateTitle() {
		TitleTable table = TitleTable.getInstance();
		
		boolean madeTitle = (boolean) table.createtitle("NewOne", "The Newest");
		
		assertEquals(true, madeTitle);
	}
	
	@Test
	public void TestCreateTitleFail() {
		TitleTable table = TitleTable.getInstance();
		
		table.createtitle("LOTR", "Lord of the Rings");
		boolean madeTitle = (boolean) table.createtitle("LOTR", "Lord of the Rings");
		
		assertEquals(false, madeTitle);
	}
	
	@Test
	public void TestLookup() {
		TitleTable table = TitleTable.getInstance();
		
		table.createtitle("LOTR", "Lord of the Rings");
		boolean foundTitle = table.lookup("LOTR");
		
		assertEquals(true, foundTitle);
	}
	
	@Test
	public void TestLookupFail() {
		TitleTable table = TitleTable.getInstance();
		
		boolean foundTitle = table.lookup("NotThere");
		
		assertEquals(false, foundTitle);
	}
	
	@Test
	public void TestDelete() {
		TitleTable table = TitleTable.getInstance();
		
		table.createtitle("LOTR", "Lord of the Rings");
		String deleteResult = (String)table.delete("LOTR");
		
		assertEquals("success", deleteResult);
	}
	
	@Test
	public void TestDeleteOnLoan() {
		TitleTable table = TitleTable.getInstance();
		
		String deleteResult = (String)table.delete("9781442668584");
		
		assertEquals("Active Loan Exists", deleteResult);
	}
	
	@Test
	public void TestDeleteDNE() {
		TitleTable table = TitleTable.getInstance();
		
		String deleteResult = (String)table.delete("NotThere");
		
		assertEquals("The Title Does Not Exist", deleteResult);
	}

}
