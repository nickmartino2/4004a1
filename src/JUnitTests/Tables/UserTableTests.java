package JUnitTests.Tables;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.ItemTable;
import server.logic.tables.UserTable;

public class UserTableTests {

	@Test
	public void TestCreateUser() {
		UserTable table = UserTable.getInstance();
		
		boolean madeUser = (boolean) table.createuser("nick50@email.com", "password2");
		
		assertEquals(true, madeUser);
	}
	
	@Test
	public void TestCreateUserFail() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@email.com", "password");
		boolean madeUser = (boolean) table.createuser("nick@email.com", "password");
		
		assertEquals(false, madeUser);
	}
	
	@Test
	public void TestLookup() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@email.com", "password");
		
		boolean found = table.lookup(0);
		
		assertEquals(true, found);
	}
	
	@Test
	public void TestLookupFail() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@email.com", "password");
		
		boolean found = table.lookup(10);
		
		assertEquals(false, found);
	}
	
	@Test
	public void TestCheckUser() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@email.com", "password");
		int result = table.checkUser("nick@email.com", "password");
		
		assertEquals(0, result);
	}
	
	@Test
	public void TestCheckUserFail2() {
		UserTable table = UserTable.getInstance();
		
		int result = table.checkUser("nick50@email.com", "password");
		
		assertEquals(2, result);
	}
	
	@Test
	public void TestCheckUserFail1() {
		UserTable table = UserTable.getInstance();
		
		table.createuser("nick@email.com", "password");
		int result = table.checkUser("nick@email.com", "notpassword");
		
		assertEquals(1, result);
	}
	
	@Test
	public void TestDeleteUser() {
		UserTable table = UserTable.getInstance();
		
		String result = (String) table.delete(1);
		
		assertEquals("success", result);
	}
	
	@Test
	public void TestDeleteUserOutstandingFee() {
		UserTable table = UserTable.getInstance();
		
		String result = (String) table.delete(0);
		
		assertEquals("Outstanding Fee Exists", result);
	}
	
	@Test
	public void TestDeleteUserDNE() {
		UserTable table = UserTable.getInstance();
		
		String result = (String) table.delete(12);
		
		assertEquals("The User Does Not Exist", result);
	}

}
