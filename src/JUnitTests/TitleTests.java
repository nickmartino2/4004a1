package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import server.logic.model.Title;

public class TitleTests {

	@Test
	public void TestCreateTitle() {
		String ISBN = "500";
		String bookTitle = "999";
		
		Title title = new Title(ISBN, bookTitle);
		
		assertEquals(ISBN, title.getISBN());
		assertEquals(bookTitle, title.getBooktitle());
	}
	
	@Test
	public void TestIBSN() {
		String ISBN = "500";
		String bookTitle = "999";
		
		Title title = new Title("", bookTitle);
		title.setISBN(ISBN);
		
		assertEquals(ISBN, title.getISBN());
	}
	
	@Test
	public void TestBookTitle() {
		String ISBN = "500";
		String bookTitle = "999";
		
		Title title = new Title(ISBN, "");
		title.setBooktitle(bookTitle);
		
		assertEquals(bookTitle, title.getBooktitle());
	}
	
	@Test
	public void TestToString() {
		String ISBN = "500";
		String bookTitle = "999";
		String returnString = "["+ISBN+","+bookTitle+"]";
		
		
		Title title = new Title(ISBN, bookTitle);
		
		assertEquals(returnString, title.toString());
	}

}
