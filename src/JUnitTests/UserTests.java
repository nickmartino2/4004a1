package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import server.logic.model.User;

public class UserTests {

	@Test
	public void TestCreateUnit() {
		int userID = 4;
		String username = "1000";
		String password = "999";
		
		User user = new User(userID, username, password);
		
		assertEquals(userID, user.getUserid());
		assertEquals(username, user.getUsername());
		assertEquals(password, user.getPassword());
	}
	
	@Test
	public void TestUserID() {
		int userID = 4;
		String username = "1000";
		String password = "999";
		
		User user = new User(0, username, password);
		user.setUserid(userID);
		
		assertEquals(userID, user.getUserid());
	}
	
	@Test
	public void TestUsername() {
		int userID = 4;
		String username = "1000";
		String password = "999";
		
		User user = new User(userID, "", password);
		user.setUsername(username);
		
		assertEquals(username, user.getUsername());
	}
	
	@Test
	public void TestPassword() {
		int userID = 4;
		String username = "1000";
		String password = "999";
		
		User user = new User(userID, username, "");
		user.setPassword(password);
		
		assertEquals(password, user.getPassword());
	}
	
	@Test
	public void TestToString() {
		int userID = 4;
		String username = "1000";
		String password = "999";
		String returnString = "["+userID+","+username+","+password+"]";
		
		
		User user = new User(userID, username, password);
		
		assertEquals(returnString, user.toString());
	}

}

